import handler from "../libs/handler-lib";
import dynamoDb from "../libs/dynamodb-lib";
import { v1 as uuidv1 } from 'uuid';

export const main = handler(async (event, context) => {
    let appointmentId = uuidv1(); // Creates unique identifier
	const params = {
		TableName: process.env.tableNameAppointments,
		/*
        Item      - contians attributes of the meeting created
        appointmentId - contains the unique identifier of appointment
        meetingId - Chime meeting
        clientId - defines the client id /key/ name that booked the meeting
	    userId  - cognito pool identity id of the authenticated lawyer
        contact_number - contact number of client
        location - location of client
        concern - brief description of legal problem
        meetingDate - available date of the lawyer that was chosen
	    */
		Item: {
            appointmentId: appointmentId,
            clientId: event.queryStringParameters.clientId,
            userId: event.queryStringParameters.userId,
            contact_number: event.queryStringParameters.contact_number,
            email: event.queryStringParameters.email,
            location: event.queryStringParameters.location,
            concern: event.queryStringParameters.concern,
            meetingDate: event.queryStringParameters.meetingDate,
            timeslot: event.queryStringParameters.timeslot
		}
	};
    await dynamoDb.put(params);
    console.log(params.Item);
	return params.Item;
});

