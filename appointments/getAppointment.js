//This API Function will be used to retrieve the meeting ID created by the client
import handler from "../libs/handler-lib";
import dynamoDb from "../libs/dynamodb-lib";

export const main = handler(async (event, context) => {
	const params = {
		TableName: process.env.tableNameAppointments,
		/*
	    Key     - defines the primary key and the sorting key to be obtained
	    meetingId - path parameter
	    */
		Key: {
			appointmentId: event.pathParameters.id
		}
	};
	const result = await dynamoDb.get(params);
	console.log(result.Item);
	if(!result.Item) {
		throw new Error("Meeting ID not found.");
	}
    return result.Item; //Retrives the full details of the appointment

});
