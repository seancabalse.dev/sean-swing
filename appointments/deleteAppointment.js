import handler from "../libs/handler-lib";
import dynamoDb from "../libs/dynamodb-lib";

export const main = handler(async (event, context) => {
  const params = {
    TableName: process.env.tableNameAppointments,
    // 'Key' defines the partition key and sort key of the item to be removed
    // - 'userId': Identity Pool identity id of the authenticated user
    // - 'meetingId': path parameter
    Key: {
      appointmentId: event.pathParameters.id
    }
  };

  await dynamoDb.delete(params);
  console.log({status: true});
  return { status: true };
});