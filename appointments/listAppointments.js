import handler from "../libs/handler-lib";
import dynamoDb from "../libs/dynamodb-lib";

export const main = handler(async(event, context) => {
	const params = {
		TableName: process.env.tableNameAppointments,
		IndexName: process.env.indexName,

		/*
	    KeyConditionExpression    - defines the condition for the query
	    'userId = :userId'        - means you can only return meetingIds
    						        under the same primary key userId
    	ExpressionAttributeValues - define values in the condition
    	':userId'                 - cognito identity pool id of the authenticated user
    	*/
		KeyConditionExpression: "userId = :userId",
		FilterExpression: "userId = :userId",
		ExpressionAttributeValues: {
			":userId": event.queryStringParameters.userId
		}
	};
	const result = await dynamoDb.scan(params);
	console.log(result.Items);
	return result.Items;
});

